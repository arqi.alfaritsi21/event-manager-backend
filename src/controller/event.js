const helper = require("../helper");
const { getAll, create, search } = require("../model/event");

module.exports = {
  getAll: async (request, response) => {
    try {
      const result = await getAll();
      return helper.response(response, 200, "Success", result);
    } catch (error) {
      return helper.response(response, 400, "Bad Request");
    }
  },
  create: async (request, response) => {
    const { title, location, participant, date, note, image } = request.body;

    const setData = {
      title,
      location,
      participant,
      date,
      note,
      image,
      created_at: new Date()
    };
    try {
      const result = await create(setData);
      return helper.response(response, 200, "Success", result);
    } catch (error) {
      return helper.response(response, 400, "Bad Request");
    }
  },
  search: async (request, response) => {
    const { data } = request.body;

    try {
      const result = await search(data);
      return helper.response(response, 200, "Success", result);
    } catch (error) {
      return helper.response(response, 400, "Bad Request");
    }
  },
  uploadFile: async (request, response) => {
    try {
      console.log(request.file);
      const path = request.file.path.split("\\").join("/")
      return helper.response(response, 200, "Success", { data: path });
    } catch (error) {
      return helper.response(response, 400, "Bad Request");
    }
  },
};
