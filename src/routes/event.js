const router = require("express").Router();
const upload = require("../middleware/multer");
const { getAll, create, search, uploadFile } = require("../controller/event");

router.get("/", getAll);
router.post("/", create);
router.post("/search", search);
router.post("/upload", upload, uploadFile);




module.exports = router;
